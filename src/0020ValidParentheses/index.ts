export function isValid(s: string): boolean {
  let stack: string[] = [];
  for (let ch of s) {
    if (ch === "(" || ch === "[" || ch === "{") {
      stack.push(ch);
    } else {
      let val = stack.pop();
      if (
        (val === "(" && ch === ")") ||
        (val === "[" && ch === "]") ||
        (val === "{" && ch === "}")
      ) {
        continue;
      }
      return false;
    }
  }
  if (stack.length > 0) {
    return false;
  }
  return true;
}
