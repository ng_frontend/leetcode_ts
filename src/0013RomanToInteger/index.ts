export function romanToInt(s: string): number {
    let result = returnNumberFromRomanNumber(s[s.length - 1]);
    for(let i = s.length-2; i>=0; i--)
    {
        if(returnNumberFromRomanNumber(s[i]) < returnNumberFromRomanNumber(s[i+1]))
        {
            result -= returnNumberFromRomanNumber(s[i])
        }else{
            result += returnNumberFromRomanNumber(s[i])
        }
    }
    return result;
};


function returnNumberFromRomanNumber(s: string):number  {
    switch (s) {
        case "I": return 1;
        case "V": return 5;
        case "X": return 10;
        case "L": return 50;
        case "C": return 100;
        case "D": return 500;
        case "M": return 1000;
        default: return 0;
    }
}