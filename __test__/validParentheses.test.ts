import {isValid} from "../src/0020ValidParentheses"

describe("first test",()=>{
    it.skip("values () must return true", () =>{
        const result = isValid("()")
        expect(result).toBeTruthy()
    }),
    it.skip("values ()[]{} must return true", () =>{
        const result = isValid("()[]{}")
        expect(result).toBeTruthy()
    }),
    it.skip("values (] must return false", () =>{
        const result = isValid("(]")
        expect(result).toBeFalsy()
    }),
    it("values ([)] must return false", () =>{
        const result = isValid("([)]")
        expect(result).toBeFalsy()
    })
})