import {containsDuplicate} from "../src/0217ContainsDuplicate"

describe("Contains Duplicate tests",()=>{
    it("values [1,2,3,1] must return true", () =>{
        const result = containsDuplicate([1,2,3,1])
        expect(result).toBeTruthy()
    }),
    it("values [1,2,3,4] must return false", () =>{
        const result = containsDuplicate([1,2,3,4])
        expect(result).toBeFalsy()
    }),
    it("values [1,1,1,3,3,4,3,2,4,2] must return false", () =>{
        const result = containsDuplicate([1,1,1,3,3,4,3,2,4,2])
        expect(result).toBeTruthy()
    })
    ,
    it("values [3,3] must return true", () =>{
        const result = containsDuplicate([3,3])
        expect(result).toBeTruthy()
    })
    
})