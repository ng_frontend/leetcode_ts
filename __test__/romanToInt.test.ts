import {romanToInt} from "../src/0013RomanToInteger"

describe("Roman To Integer tests",()=>{
    it("values III must return 3", () =>{
        const result = romanToInt("III")
        expect(result).toBe(3)
    }),
    it("values LVIII must return 58", () =>{
        const result = romanToInt("LVIII")
        expect(result).toBe(58)
    }),
    it("values MCMXCIV must return 1994", () =>{
        const result = romanToInt("MCMXCIV")
        expect(result).toBe(1994)
    })
})